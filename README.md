# Secondectomy

Drupal 8 module to remove seconds from all date/time fields. This is a global module with no settings. For now, it's on or off for everything. If we ever need granularity for forms then we can add that functionality later.